package br.com.hybrida.greenalg.exception;

import br.com.hybrida.greenalg.message.MessageManager;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;

import java.util.Collection;

@Getter
@Setter
@Log4j2
public class BusinessException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	private final String message;
	private final HttpStatus httpStatus;

	public BusinessException(HttpStatus http, String string, Throwable thrwbl) {
		super(string, thrwbl);
		log.error(thrwbl.getMessage(), thrwbl);
		this.message = MessageManager.getMessage(string);
		this.httpStatus = http;
	}

	public BusinessException(HttpStatus http, String key, Object... args) {
		super(MessageManager.getMessage(key, args));
		this.message = MessageManager.getMessage(key, args);
		this.httpStatus = http;
	}

	public static IntermediateContext assertThat(boolean condition) {
		return new IntermediateContext(condition);
	}

	public static IntermediateContext assertNotNull(Object object) {
		return new IntermediateContext(object != null);
	}

	public static IntermediateContext assertNull(Object object) {
		return new IntermediateContext(object == null);
	}

	public static IntermediateContext assertNotEmpty(String string) {
		return new IntermediateContext(string != null && !string.isEmpty());
	}

	public static IntermediateContext assertNotEmpty(Collection collection) {
		return new IntermediateContext(collection != null && !collection.isEmpty());
	}

	public static class IntermediateContext {

		private final boolean assertIsFalse;

		public IntermediateContext(boolean condition) {
			this.assertIsFalse = !condition;
		}

		public void orRegistrer(HttpStatus httpStatus, String message, Object... parametros) {
			if (assertIsFalse) {
				throw new BusinessException(httpStatus, message, parametros);
			}
		}

		public void orRegistrer(String message, Object... parametros) {
			if (assertIsFalse) {
				throw new BusinessException(HttpStatus.BAD_REQUEST, message, parametros);
			}
		}
	}

}
