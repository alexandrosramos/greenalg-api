package br.com.hybrida.greenalg.service;

import br.com.hybrida.greenalg.component.CategoriaComponent;
import br.com.hybrida.greenalg.dto.CategoriaDTO;
import br.com.hybrida.greenalg.dto.parser.CategoriaParser;
import br.com.hybrida.greenalg.entity.Categoria;
import br.com.hybrida.greenalg.service.base.BaseService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Log4j2
public class CategoriaService implements BaseService {

    private final CategoriaComponent categoriaComponent;

    @Autowired
    public CategoriaService(CategoriaComponent categoriaComponent) {
        this.categoriaComponent = categoriaComponent;
    }

    @Transactional
    public List<CategoriaDTO> obterCategorias() {
        List<Categoria> categoriaList = categoriaComponent.recuperarListaCategorias();
        return CategoriaParser.fromCategoriaListToCategoriaListDTO(categoriaList);
    }
}
