package br.com.hybrida.greenalg.component;

import br.com.hybrida.greenalg.entity.Categoria;
import br.com.hybrida.greenalg.repository.CategoriaRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Log4j2
public class CategoriaComponent {

	private final CategoriaRepository categoriaRepository;

	@Autowired
	public CategoriaComponent(CategoriaRepository categoriaRepository) {
		this.categoriaRepository = categoriaRepository;
	}

	public List<Categoria> recuperarListaCategorias() {
		List<Categoria> categoriaList = categoriaRepository.findAll();
		return categoriaList;
	}
}
