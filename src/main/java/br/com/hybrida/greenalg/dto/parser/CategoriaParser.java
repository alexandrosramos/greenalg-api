package br.com.hybrida.greenalg.dto.parser;

import br.com.hybrida.greenalg.dto.CategoriaDTO;
import br.com.hybrida.greenalg.entity.Categoria;

import java.util.ArrayList;
import java.util.List;

public final class CategoriaParser {

	private CategoriaParser() {
		// vazio
	}

	public static CategoriaDTO fromCategoriaToCategoriaDTO(Categoria categoria) {

		if (categoria == null || categoria.getId() == null) {
			return null;
		}

		final CategoriaDTO categoriaDTO = new CategoriaDTO();

		categoriaDTO.setNome(categoria.getNome());
		categoriaDTO.setDescricao(categoria.getDescricao());
		categoriaDTO.setSituacao(categoria.getSituacao());

		return categoriaDTO;
	}

	public static List<CategoriaDTO> fromCategoriaListToCategoriaListDTO(List<Categoria> categoriaList){

		List<CategoriaDTO> categoriaDTOList = new ArrayList<>(categoriaList.size());

		for ( Categoria categoria : categoriaList) {
			categoriaDTOList.add(fromCategoriaToCategoriaDTO(categoria));
		}

		return categoriaDTOList;
	}

}
