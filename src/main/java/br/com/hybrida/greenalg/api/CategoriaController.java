package br.com.hybrida.greenalg.api;

import br.com.hybrida.greenalg.dto.CategoriaDTO;
import br.com.hybrida.greenalg.service.CategoriaService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@Log4j2
@RequestMapping(value = "categoria")
public class CategoriaController extends BaseController {

	@Autowired
	private CategoriaService categoriaService;

	@GetMapping("/")
	public ResponseEntity<List<CategoriaDTO>> obterCategorias() {
		log.info("Request iniciada /categoria");
		List<CategoriaDTO> categorias = categoriaService.obterCategorias();
		log.info("Request finalizada /categoria");

		return ResponseEntity.ok().body(categorias);
	}

}
