package br.com.hybrida.greenalg.api;

import br.com.hybrida.greenalg.exception.BusinessException;
import br.com.hybrida.greenalg.message.MessageManager;
import br.com.hybrida.greenalg.message.MessageUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;

@RestController
public class BaseController {

	private static final String MESSAGE = "message";

	@ExceptionHandler(Throwable.class)
	public ResponseEntity<Object> handleError(Throwable responseException) {

		BodyBuilder bodyBuilder;

		ResponseEntity<Object> retorno = null;

		if (responseException instanceof BusinessException) {
			bodyBuilder = ResponseEntity.status(((BusinessException) responseException).getHttpStatus());

			retorno = bodyBuilder.body(Collections.singletonMap(MESSAGE, responseException.getMessage()));
		} else if (responseException instanceof AccessDeniedException) {
			bodyBuilder = ResponseEntity.status(HttpStatus.UNAUTHORIZED);
			retorno = bodyBuilder
					.body(Collections.singletonMap(MESSAGE, MessageManager.getMessage(MessageUtils.ACESSO_NEGADO)));
		} else {

			bodyBuilder = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR);

			retorno = bodyBuilder
					.body(Collections.singletonMap(MESSAGE, MessageManager.getMessage(MessageUtils.ERRO_INTERNO)));
		}
		return retorno;
	}
}
