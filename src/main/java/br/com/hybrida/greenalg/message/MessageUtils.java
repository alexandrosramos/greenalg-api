package br.com.hybrida.greenalg.message;

public final class MessageUtils {

	private MessageUtils() {
		// vazio
	}

	public static final String ERRO_INTERNO = "erro.interno";
	public static final String ACESSO_NEGADO = "acesso.negado";
}
